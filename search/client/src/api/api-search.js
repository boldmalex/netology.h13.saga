const url = "http://localhost:7070/api/search";

const apiSearch = async (search) => {
    const params = new URLSearchParams({ q: search });
    const response = await fetch(`${url}?${params}`);
    if (!response.ok) throw new Error(response.statusText);
    return await response.json();
  };

export default apiSearch;