import { applyMiddleware, createStore } from "redux";
import searchReducer from "./search/search-reducer";
import createSagaMiddleware from "@redux-saga/core";
import rootSaga from "./search/search-sagas";


const sagaMiddlware = createSagaMiddleware();

const store = createStore(searchReducer, applyMiddleware(sagaMiddlware));

sagaMiddlware.run(rootSaga);

export default store;