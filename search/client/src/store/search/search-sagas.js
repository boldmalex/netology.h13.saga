import { take, put, takeLatest, call, spawn } from "redux-saga/effects";
import searchActionTypes from "./search-action-types";
import searchActions from "./search-actions";
import apiSearch from "../../api/api-search";


function* searchChangeSearchSaga() {
  while (true) {
    const action = yield take(searchActionTypes.SEARCH_CHANGE);

    if (action.payload.search.trim() === "") continue;

    yield put(searchActions.searchRequest(action.payload.search));
  }
}


function* watcherSearchRequestSaga() {
  yield takeLatest(searchActionTypes.SEARCH_REQUEST, workerSearchRequestSaga);
}


function* workerSearchRequestSaga(action) {
  try {
    const data = yield call(apiSearch, action.payload.search);
    yield put(searchActions.searchSuccess(data));
  } catch (e) {
    yield put(searchActions.searchFailure(e));
  }
}

export default function* rootSaga() {
  yield spawn(searchChangeSearchSaga);
  yield spawn(watcherSearchRequestSaga);
}
