import searchActionTypes from "./search-action-types.js";

const searchRequest = (search) => ({
  type: searchActionTypes.SEARCH_REQUEST,
  payload: { search },
});

const searchFailure = (error) => ({
  type: searchActionTypes.SEARCH_FAILURE,
  payload: { error: error },
});

const searchSuccess = (items) => ({
  type: searchActionTypes.SEARCH_SUCCESS,
  payload: { items },
});

const searchChange = (search) => ({
  type: searchActionTypes.SEARCH_CHANGE,
  payload: { search },
});

const searchActions = {
  searchRequest,
  searchFailure,
  searchSuccess,
  searchChange,
};

export default searchActions;
