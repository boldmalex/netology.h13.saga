import serviceActionTypes from "./service-action-types"

const initialState = {
    items: [],
    isLoading: false,
    error: null
}

const servicesReducer = (state = initialState, action) => {

    switch (action.type) {

        case serviceActionTypes.SERVICES_REQUEST :
            return { ...state, isLoading: true, error: null};

        case serviceActionTypes.SERVICES_FAILURE :
            const {error} = action.payload;
            return { ...state, isLoading: false, error: error};

        case serviceActionTypes.SERVICES_SUCCESS:
            const {items} = action.payload;
            return {...state, isLoading: false, error: null, items: items};

        default: 
            return state;
    }
}

export default servicesReducer;