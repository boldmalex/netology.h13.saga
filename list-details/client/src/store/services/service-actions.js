import serviceActionTypes from "./service-action-types";


const servicesRequest = () => ({
    type: serviceActionTypes.SERVICES_REQUEST    
});

const servicesSuccess = (items) => ({
    type: serviceActionTypes.SERVICES_SUCCESS,
    payload: {items:items}
});

const servicesFailure = (error) => ({
    type: serviceActionTypes.SERVICES_FAILURE,
    payload: {error:error}
});

const serviceRequest = (id) => ({
    type: serviceActionTypes.SERVICE_REQUEST,
    payload: {id:id} 
});

const serviceSuccess = (item) => ({
    type: serviceActionTypes.SERVICE_SUCCESS,
    payload: {item:item}
});

const serviceFailure = (error) => ({
    type: serviceActionTypes.SERVICE_FAILURE,
    payload: {error:error}
});


const servicesActions = {
    servicesRequest,
    servicesSuccess,
    servicesFailure,
    serviceRequest,
    serviceSuccess,
    serviceFailure
}

export default servicesActions;