import serviceActionTypes from "./service-action-types"

const initialState = {
    item: null,
    isLoading: false,
    error: null
}

const serviceReducer = (state = initialState, action) => {

    switch (action.type) {

        case serviceActionTypes.SERVICE_REQUEST :
            return { ...state, isLoading: true, error: null};

        case serviceActionTypes.SERVICE_FAILURE :
            const {error} = action.payload;
            return { ...state, isLoading: false, error: error};

        case serviceActionTypes.SERVICE_SUCCESS:
            const {item} = action.payload;
            return {...state, isLoading: false, error: null, item: item};

        default: 
            return state;
    }
}

export default serviceReducer;