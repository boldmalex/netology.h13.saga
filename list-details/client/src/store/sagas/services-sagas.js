import { call, spawn, put, takeEvery } from "redux-saga/effects";
import serviceActionTypes from "../services/service-action-types";
import apiServices from "../../api/api-services";
import servicesActions from "../services/service-actions";

function* watcherServicesRequestSaga() {
  yield takeEvery(
    serviceActionTypes.SERVICES_REQUEST,
    workerServicesRequestSaga
  );
}


function* workerServicesRequestSaga(action) {
  try {
    const data = yield call(apiServices.servicesGet);
    yield put(servicesActions.servicesSuccess(data));
  } catch (e) {
    yield put(servicesActions.servicesFailure(e));
  }
}


function* watcherServiceRequestSaga() {
  yield takeEvery(serviceActionTypes.SERVICE_REQUEST, workerServiceRequestSaga);
}


function* workerServiceRequestSaga(action) {
  try {
    const data = yield call(apiServices.serviceGet, action.payload.id);
    yield put(servicesActions.serviceSuccess(data));
  } catch (e) {
    yield put(servicesActions.serviceFailure(e));
  }
}

export default function* rootSaga() {
    yield spawn(watcherServicesRequestSaga);
    yield spawn(watcherServiceRequestSaga);
  }