import { applyMiddleware, combineReducers, createStore } from "redux";
import serviceReducer from "./services/service-reducer";
import servicesReducer from "./services/services-reducer";
import createSagaMiddleware from "@redux-saga/core";
import rootSaga from "../store/sagas/services-sagas";

const reducer = combineReducers({servicesReducer, serviceReducer});

const sagaMiddlware = createSagaMiddleware();

const store = createStore(reducer, applyMiddleware(sagaMiddlware));

sagaMiddlware.run(rootSaga);

export default store;
