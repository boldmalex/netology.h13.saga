const url = "http://localhost:7070/api/services";

const servicesGet = async () => {
    const response = await fetch(`${url}`);
    if (!response.ok) throw new Error(response.statusText);
    return await response.json();
  };

const serviceGet = async (id) => {
    const response = await fetch(`${url}/${id}`);
    if (!response.ok) throw new Error(response.statusText);
    return await response.json();
  };

  
const apiServices = {
    servicesGet,
    serviceGet
};

export default apiServices;