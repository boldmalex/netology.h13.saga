import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import store from "../../../store/store";
import servicesActions from "../../../store/services/service-actions";
import ErrorMessage from "../../error-message/error-message";
import { useNavigate } from "react-router-dom";
import { SpinnerCircularSplit } from "spinners-react";


const ServicesList = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const store = useSelector((store) => store.servicesReducer);

  const loadData = () => {
    dispatch(servicesActions.servicesRequest());
  };

  useEffect(() => {
    loadData();
  }, []);

  const handleItemClick = (id) => {
    navigate(`/${id}/details`);
  };

  const showItem = (item) => {
    return (
      <li
        key={item.id}
        className="list-item"
        onClick={() => handleItemClick(item.id)}
      >
        {item.name} - {item.price}
      </li>
    );
  };

  if (store.isLoading) return <SpinnerCircularSplit/>;
  else if (store.error)
    return (
      <ErrorMessage
        text="Произошла ошибка!"
        btnText="Повторить запрос"
        btnHandler={loadData}
      />
    );
  else if (!store.items) return null;
  else return <ul>{store.items.map((item) => showItem(item))}</ul>;
};

export default ServicesList;
