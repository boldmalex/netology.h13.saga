import React from "react";

const ErrorMessage = ({ text, btnText, btnHandler }) => {
  return (
    <div className="error">
      {text}
      <button className="button-error" onClick={btnHandler}>
        {btnText}
      </button>
    </div>
  );
};

export default ErrorMessage;
